<?php
// recuperer les trois paramètres (y'en a 4 parce que le lien en est un car =0)
// les mettres dans une variable
// condition selon l'opérateur 


// SI les paramètres sont pas égal à 4, indiquer que c'est une erreur.
// SINON définir les 3 autres variables en enlevant les espaces.
//SI la varaible 1 et 2 son des chiffres on peut commencer les opérations.
// -> SI l'opérateur est un +, faire le calcule et afficher le resultat et ainsi de suite avec les autres types d'opérateur
// Pour la division, on ne peut pas diviser 0 par 0, alors on dit que si la deuxieme valeur est 0 on affiche quand meme 0.
// SINON, si dans toutes ses opérations il n'y a pas de chiffres, on affiche un message d'erreur. 

if ($argc !== 4) {
    echo "Incorrect Parameters";
    echo "\n";
} else {
    $var1 = trim($argv[1]);
    $op = trim($argv[2]);
    $var2 = trim($argv[3]);

    if ((is_numeric($var1)) && (is_numeric($var2))) {

        if ($op == "+") {
            $res = $var1 + $var2;
            echo $res;
            echo "\n";
        }

        if ($op == "-") {
            $res = $var1 - $var2;
            echo $res;
            echo "\n";
        }

        if ($op == "*") {
            $res = $var1 * $var2;
            echo $res;
            echo "\n";
        }

        if ($op == "/") {
            if ($var2 == 0) {
                echo 0;
                echo "\n";
            } else {
                $res = $var1 / $var2;
                echo $res;
                echo "\n";
            }
        }

        if ($op == "%") {
            $res = $var1 % $var2;
            echo $res;
            echo "\n";
        } 
    } else {
        echo "Incorrect Parameters";
        echo "\n";
    }
}
