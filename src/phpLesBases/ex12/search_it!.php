<?php


//donner le nom des clefs aux index
// appeler la valeur avec les clefs
//toujours afficher la valeur de toto
//toto n'a pas de valeur, pour l'avant dernière
// pour la dernière on affche pas la valeur de 0 car elle en a pas

//si il y a moins de trois argument ne rien mettre
if ($argc < 3) {
    exit();
}
// je créer un tableau vide
$tab = [];

//je donne un nom au premier argument qui est toto
$mot = $argv[1];

//je récupère les valeurs du tableau
$array = array_slice($argv, 2);
// var_dump ca sert à afficher sous forme de tableau
// var_dump($array);

// je demande d'afficher les paramètres et les valeurs sous forme de tableau
foreach ($array as $value) {

    // j'explose la chaine du tableau donc : et sa valeur.
    $tab = explode(':', $value);

    // empty détermine si une variable est vide, le ! indique le contraire, donc ici on vérifie si le premier tableau contient une variable donc on la nomme en visant le paramètre 1.
    if (!empty($tab[1])) {
        $val = $tab[1];
    }

    // je nomme ma clef en visant le paramètre 0.
    $key = $tab[0];
    // je nomme le tableau avec les clefs.
    $key_tab[] = $key;

    // si toto est une clef et qu'elle est pas vide je créée un nouveau tableau qui contient les variables.
    if ($mot == $key) {
        if (!empty($val)) {
            $val_tab[] = $val;
        }
    }
}

//si les variables sont différentes de null on affiche la dernière variable du tableau.
if (isset($val_tab)) {
    echo end($val_tab);
    echo "\n";
}
