<?php
// Je récupère le seul argument
// J'utilise une fonction pour lire un autre fichier "page.html"
$text = file_get_contents ($argv[1]);
// j'ai cherché à trouver les liens "a href" en les chevrons (regex) et mis le tableau des liens
preg_match_all ("/<a.*>.*<\/a>/",$text, $tabliens);
// J'ai une boucle pour chaque lien du tableau
foreach ($tabliens[0] as $lien ){
// Je cherche le text qui se trouve entre les chevrons
    preg_match("/>.*</",$lien,$tab);
// je met du vide entre les chevrons
    $final = preg_replace("/<.*>/",'',$tab[0]);
// Je met le texte en majuscule
    $gras=strtoupper($final);
// Je remplace le text du lien en gras 
    $text=str_replace($final,$gras,$text);
}

// j'ai cherché à trouver les textes avec title=" "
preg_match_all ("/title=\".*\"/",$text, $tabliens);
foreach ($tabliens[0] as $title ){
    $mot=str_replace("title","",$title);
    $mot=strtoupper($mot);
    $mot="title".$mot;
    $text=str_replace($title,$mot,$text);

    }

    echo $text;